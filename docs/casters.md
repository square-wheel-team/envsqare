# Casters

## [What is "Cast"?](https://youtube.com/watch?v=Kbj4bulZX2Y)

<p class="section-quote">
"Baby don't read me, don't read me, I'm raw."
</p>

In Computer Science, **[type casting](https://en.wikipedia.org/wiki/Type_conversion)**, often shortened to just **casting**, refers to the process of changing an expression from one data type to another.

For example:

* An integer like **5** can be *casted* to the string **'5'**.
* A string like **'Hello world!'** can be *casted* to an array of integers, each representing a symbol.
* A floating point number like **7.34** can be *casted* to the integer **7** by truncating it.

Should you need any further information regarding this topic, here's a brief explanation of [casting in Python](https://www.w3schools.com/python/python_casting.asp). And of course, there's always [Google](https://bfy.tw/QtOZ).


## Casting environment variables

<p class="section-quote">
"Tantum me nihil scire scio."
<br>
I know only that I know nothing.
</p>

You may be very well wondering what does converting a variable from one type to another have to do with a library intended to read environment files, load variables and fetch them later.
I would reply: "**Everything**".

Environment variables are usually stored as strings, hence when reading them from our Python code, we usually get the data represented as a string (for instance, using *os.getenv*).

This feature, which draws inspiration from the popular library [environs](https://github.com/sloria/environs), lets you fetch a variable from the environment in whichever format you desire. You get the string and **cast** it to a different representation.

To be clearer, using an **IntCaster** would be roughly equivalent to the following code:

```python
import os

my_casted_var = int(os.getenv('SOME_NUMERIC_VAR', '0'))
assert isinstance(my_casted_var, int)  # Would succeed
```

## Custom Casters

<p class="section-quote">
"El talento nace en cualquier persona que se sienta capaz de volar con sus ideas."
<br>
Talent sprouts from any person whose ideas take flight.
</p>

This library focuses on extensibility, meaning you are encouraged to create your own casters. It's as easy as subclassing the abstract **envsqare.casters.Caster** class.

### Caster for a custom data type

You can write your own caster to read an environment variable directly into a class you've created beforehand.

``` python linenums="1"
from envsqare.casters import Caster


class CustomCounter:
    def __init__(self, initial_count: int):
        self.count = initial_count

    def add(self, amount: int = 1):
        self.count += amount

    def substract(self, amount: int = 1):
        self.add(-amount)


class CounterCaster(Caster):
    def cast(self, value: str) -> CustomCounter:
        return CustomCounter(int(value))
```

### Caster with a specific format

Fetching a variable from the environment into your own class isn't the only use case for a caster. You may also want to use the default data type but provide a specific formatting.

``` python linenums="1"
from envsqare.casters import Caster


class CasterWithFormat(Caster):
    """Will enclose the variable's value between double low-dashes."""

    def cast(self, value: str) -> str:
        return f'__{value}__'
```


## Out-of-the-box casters

<p class="section-quote">
"The standard library saves programmers from having to reinvent the wheel."
</p>

The following is a list of all casters provided within this same library.

* **Caster**: Abstract class from which all others should inherit.
* **IntCaster**: Returns an integer. Can take the base as a parameter on init: `#!python IntCaster(16)` would read strings as if they were hexadecimal numbers.
* **StringCaster**: Returns a string. If the value has been read as bytes, it is decoded using the *encoding* parameter provided on init: `#!python StringCaster('latin-1')`.
* **BytesCaster**: Returns a bytes stream. It takes an *encoding* parameter on init: `#!python BytesCaster('latin-1')`.
* **ListCaster**: Returns a list. It takes a splitter and an element caster on init: `#!python ListCaster(splitter='|', element_caster=IntCaster)` would cast `#!python '1|2|3|4|5'` into `#!python [1, 2, 3, 4, 5]`.
* **GeneratorCaster**: A special case of the ListCaster which returns a generator instead of a list. Takes the same parameters.
* **DictFromPrefixCaster**: Reads all variables from the environment starting with the provided prefix and returns a dictionary containing them. It takes the *prefix_splitter* parameter on init: `#!python DictFromPrefixCaster('/')` would cast `#!bash SOME_PREFIX/VALUE=5` into `#!python {'VALUE': '5'}`.
* **MongoCaster**: It subclasses the DictFromPrefixCaster and returns a Mongo URI (i.e.: mongodb://user:password@localhost:port/db?authSource=admin&ssl=true).
