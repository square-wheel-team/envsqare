#! /bin/env bash

FOLDER_NAME='docs_translations'
FOLDER_LENGTH=$((${#FOLDER_NAME} + 1))  # Add the slash
LOCAL_DOCS_FOLDER='docs'
DOCS_FOLDER='build_docs/'
DEFAULT_FOLDER='default'

sed -i -r "s%docs_dir: (.*)%docs_dir: $DOCS_FOLDER%g" mkdocs.yml
mkdir -p $DOCS_FOLDER

for RAW_FOLDER in $(ls -d docs_translations/*/); do
    DOCS_LANG_FOLDER=${RAW_FOLDER:FOLDER_LENGTH:-1}  # Remove the folder and the trailing slash
    cp -r $RAW_FOLDER* $DOCS_FOLDER
    poetry run mkdocs build --site-dir "public/$DOCS_LANG_FOLDER"
done

cp -r "public/$DEFAULT_FOLDER/"* "public/"
rm -rf "public/$DEFAULT_FOLDER"
rm -rf build_docs/

sed -i -r "s%docs_dir: (.*)%docs_dir: $LOCAL_DOCS_FOLDER%g" mkdocs.yml
