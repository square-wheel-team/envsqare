# ¡Bienvenido a la documentación de EnvsQare!

Because at S**Q**uare Wheel, we **C**are about environment and aim to make it less s**C**aring.

For source code, please visit the [GitLab repository](https://gitlab.com/square-wheel-team/envsqare).

## Compatibility

* **Python 3.6**: With *dataclasses* package.
* **Python 3.7**
* **Python 3.8**
* **Python 3.9**

## Quickstart

A quick and simple example to start learning hands-on. Try to understand how everything works with these pieces of code.

### Installation

<div class="flex-container">

<div class="flex-content flex-2">
<p class="codeblock-label">Raw PIP command</p>
```bash
pip install envsqare
```
</div>

<div class="flex-content flex-2">
<p class="codeblock-label">With <a href="https://python-poetry.org/">Poetry</a></p>
```bash
poetry add envsqare
```
</div>

</div>


### Usage

<p class="codeblock-label">Initializing</p>

``` python linenums="1"
from envsqare import Environment


env = Environment()
```

<p class="codeblock-label">Loading variables from environment file</p>

``` python linenums="5"
env.read_env()  # Will read from ./.env
env.read_env('some-folder/some-file.env')  # Will read from ./some-folder/some-file.env
```

<p class="codeblock-label">Reading variables from environment</p>

``` python linenums="5"
env.INT('INT_VAR_NAME')  # Value: 5
env.STR('INT_VAR_NAME')  # Value: '5'
env('INT_VAR_NAME')  # Value: '5'
env.LIST('LIST_VAR_NAME')  # Value: ['This', 'is', 'a', 'list']
env('LIST_VAR_NAME')  # Value: 'This,is,a,list'
```


#### Full example

<p class="codeblock-label"><strong>.env</strong> file</p>

``` bash linenums="1"
MOVIE_NAME="Star Wars: A New Hope"
FANS_COUNT=10000000
MAIN_CHARACTER="Luke Skywalker"
SHORT_TITLES="Star Wars Episode 4,Star Wars 1,The First Star Wars"
```

<p class="codeblock-label"><strong>main.py</strong> file</p>

``` python linenums="1"
from envsqare import Environment


env = Environment()
env.read_env()

movie_name = env('MOVIE_NAME')
fans_count = env.INT('FANS_COUNT')
main_character = env.STR('MAIN_CHARACTER')
short_titles = env.LIST('SHORT_TITLES')

print(
    f'My favourite movie is {movie_name} starring {main_character}.\n'
    f'It is not a surprise since there are over {fans_count} people who would agree.\n'
    f"Other titles for the movie are: {', '.join(short_titles)}."
)
```

<p class="codeblock-label">Output</p>

<pre class="pseudocode">
My favourite movie is <strong>Star Wars: A New Hope</strong> starring <strong>Luke Skywalker</strong>.
It is not a surprise since there are over <strong>10000000</strong> people who would agree.
Other titles for the movie are: <strong>Star Wars Episode 4, Star Wars 1, The First Star Wars</strong>.
</pre>
