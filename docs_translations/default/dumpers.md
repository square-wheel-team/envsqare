# Dumpers

## [Please go setting my vars](https://www.youtube.com/watch?v=z0qW9P-uYfM)

<p class="section-quote">
"I can stringify... Right from the start I wrangle your style."
</p>

In computing [to dump](https://dictionary.cambridge.org/us/dictionary/english/dump) means storing information from the computer's memory into another place.

Even though it may not be the exact term for this process, we will be using it refering to the contrary of casting.
For us, **to dump** will mean something alike to: *"to turn any variable into a string"*.


## Dumping into the Environment

<p class="section-quote">
"A current under sea picked his bones in whispers as he rose and fell."
</p>

As aforementioned, environment variables are usually strings. Nonetheless, we might want to store there one of the values we've been using in our code (or read from a JSON environment file), for example an array/list.

Therefore the question arises: how can we store `#!python ['some', 'list', 'value']` into the variable `#!bash MY_LIST` in a way which can be later on retrieved as a list?

The **Dumper** will be responsible for casting a specific data type into a string. Then it could be retrieved and parsed into an iterable again with a **[Caster](/casters/#casters)**, this is the close relationship between them.


## Custom Dumpers

<p class="section-quote">
"To be creative means to be in love with life. You can be creative only if you love life enough that you want to enhance its beauty."
</p>

This library focuses on extensibility, meaning you are encouraged to create your own dumpers. It's as easy as subclassing the abstract **envsqare.dumpers.Dumper** class.


### Dumper for a custom data type

You can set an environment variable to an instance of a custom class you've created if you set up the right dumper.

``` python linenums="1"
from envsqare.dumpers import Dumper


class CustomCounter:
    def __init__(self, initial_count: int):
        self.count = initial_count

    def add(self, amount: int = 1):
        self.count += amount

    def substract(self, amount: int = 1):
        self.add(-amount)


class CounterDumper(Dumper):
    def dump(self, value: CustomCounter) -> str:
        return str(value.count)
```

### Dumper with a specific format

Just as with [casters](/casters/#caster-with-a-specific-format), you can also write a **Dumper** to store the variable's value with any format you'd like.

``` python linenums="1"
from envsqare.dumpers import Dumper


class DumperWithFormat(Dumper):
    """Will enclose the variable's value between double low-dashes before setting it in the environment."""

    def dump(self, value: str) -> str:
        return f'__{value}__'
```


## Out-of-the-box dumpers

<p class="section-quote">
"A man who dares to waste one hour of time has not discovered the value of life."
</p>

The following is a list of all dumpers provided within this same library.

* **Dumper**: Abstract class from which all others should inherit.
* **GenericDumper**: Cast the provided value into a string by calling str(value).
* **ListDumper**: Join all elements from a list into a string. It takes the joiner and element_dumper as parameters on init: `#!python ListDumper(joiner='|', element_dumper=lambda x: f'__{x}__')` would turn `#!python ['first', 'second']` into `#!python '__first__|__second__'`.
