import os
from typing import Generator

import pytest

from envsqare.casters import (
    BytesCaster, BoolCaster, Caster, DictFromPrefixCaster, GeneratorCaster, IntCaster, ListCaster, MongoCaster,
    MongoCredentials, StringCaster
)


class TestGenericCaster:
    def test_no_var_name_without_default(self):
        class MyCaster(Caster):
            def cast(self, value: str):
                return value
        caster = MyCaster()
        with pytest.raises(TypeError):
            caster()

    def test_no_var_name_with_default(self):
        class MyCaster(Caster):
            DEFAULT_VAR_NAME = 'VAR'
            def cast(self, value: str):
                return value
        caster = MyCaster()
        assert caster() is None

    def test_caster_default_value(self):
        class MyCaster(Caster):
            def cast(self, value: str):
                return value
        caster = MyCaster()
        assert caster('VAR') is None
        assert caster('VAR', 5) == 5


class TestBoolCaster:
    def test_truthy_default(self):
        caster = BoolCaster()
        casted_value = caster.cast(next(v for v in caster.truthy_values))
        assert isinstance(casted_value, bool)
        assert casted_value

    def test_truthy_custom(self):
        caster = BoolCaster({'verdadero', 'verdad'})
        casted_value = caster.cast('verdad')
        assert isinstance(casted_value, bool)
        assert casted_value

    def test_falsy(self):
        casted_value = BoolCaster().cast('false')
        assert isinstance(casted_value, bool)
        assert not casted_value

        casted_value = BoolCaster().cast(None)
        assert isinstance(casted_value, bool)
        assert not casted_value


class TestIntCaster:
    def test_default_base_10(self):
        casted_value = IntCaster().cast('5')
        assert isinstance(casted_value, int)
        assert casted_value == 5

    def test_other_base(self):
        casted_value = IntCaster(base=16).cast('A')
        assert isinstance(casted_value, int)
        assert casted_value == 0xA


class TestStringCaster:
    def test_raw_string(self):
        casted_value = StringCaster().cast('Test')
        assert isinstance(casted_value, str)
        assert casted_value == 'Test'

    def test_default_encoding(self):
        casted_value = StringCaster().cast(b'Test')
        assert isinstance(casted_value, str)
        assert casted_value == b'Test'.decode('utf-8')

    def test_other_encoding(self):
        text_value = 'Some text with tildes ~ á'
        casted_value = StringCaster(encoding='latin-1').cast(text_value.encode('latin-1'))
        assert isinstance(casted_value, str)
        assert casted_value == text_value


class TestBytesCaster:
    def test_default_encoding(self):
        casted_value = BytesCaster().cast('Test')
        assert isinstance(casted_value, bytes)
        assert casted_value == 'Test'.encode('utf-8')

    def test_other_encoding(self):
        text_value = 'Some text with tildes ~ á'
        casted_value = BytesCaster(encoding='latin-1').cast(text_value)
        assert isinstance(casted_value, bytes)
        assert casted_value == text_value.encode('latin-1')


class TestListCaster:
    default_list_example = '1,2,3'
    default_caster = ListCaster()

    def test_default_splitter(self):
        assert self.default_caster.splitter == ','
        assert len(self.default_caster.cast(self.default_list_example)) == 3

    def test_default_element_caster(self):
        assert self.default_caster.element_caster == str
        for casted_element in self.default_caster.cast(self.default_list_example):
            assert isinstance(casted_element, str)

    def test_set_splitter(self):
        caster = ListCaster()
        new_splitter = '|'
        caster.set_splitter(new_splitter)
        assert caster.splitter == new_splitter
        new_splitter_list = self.default_list_example.replace(self.default_caster.splitter, new_splitter)
        assert len(caster.cast(new_splitter_list)) == 3

    def test_set_element_caster(self):
        caster = ListCaster()
        new_element_caster = int
        caster.set_element_caster(new_element_caster)
        assert caster.element_caster == new_element_caster
        for casted_element in caster.cast(self.default_list_example):
            assert isinstance(casted_element, int)

    def test_cast(self):
        casted_list = self.default_caster.cast(self.default_list_example)
        assert isinstance(casted_list, list)
        assert len(casted_list) == 3
        assert casted_list, ['1', '2' == '3']


class TestGeneratorCaster:
    def test_remains_subclass(self):
        assert issubclass(GeneratorCaster, ListCaster)

    def test_cast(self):
        casted_value = GeneratorCaster().cast('1,2,3,4')
        assert isinstance(casted_value, Generator)
        i = 1
        while i <= 4:
            assert next(casted_value) == str(i)
            i += 1


class TestDictFromPrefixCaster:
    def test_cast(self):
        assert DictFromPrefixCaster().cast('VALUE') == 'VALUE'

    def test_call_default_splitter(self):
        os.environ['PREFIX_COUNT'] = '5'
        os.environ['PREFIX_NAME'] = 'TEST'
        casted_value = DictFromPrefixCaster()('PREFIX')
        assert isinstance(casted_value, dict)
        assert casted_value.get('COUNT') == '5'
        assert casted_value.get('NAME') == 'TEST'
        os.environ.pop('PREFIX_COUNT')
        os.environ.pop('PREFIX_NAME')

    def test_call_custom_splitter(self):
        os.environ['PREFIX/COUNT'] = '5'
        os.environ['PREFIX/NAME'] = 'TEST'
        casted_value = DictFromPrefixCaster(prefix_splitter='/')('PREFIX')
        assert isinstance(casted_value, dict)
        assert casted_value.get('COUNT') == '5'
        assert casted_value.get('NAME') == 'TEST'
        os.environ.pop('PREFIX/COUNT')
        os.environ.pop('PREFIX/NAME')


class TestMongoCaster:
    # pylint: disable=protected-access
    caster = MongoCaster()
    empty_credentials = {k: None for k in MongoCredentials}
    filled_credentials = {
        MongoCredentials.USERNAME: 'username',
        MongoCredentials.PASSWORD: 'password',
        MongoCredentials.HOST: 'localhost',
        MongoCredentials.PORT: '27017',
        MongoCredentials.DATABASE: 'test-database',
        MongoCredentials.PARAMS: 'authSource=admin&ssl=true'
    }

    def test_to_camel_case_empty_string(self):
        example_string = ''
        assert self.caster._to_camel_case(example_string) == ''

    def test_to_camel_case_no_underscores(self):
        example_string = 'someString'
        assert self.caster._to_camel_case(example_string) == example_string

    def test_to_camel_one_underscore(self):
        example_string = 'some_string'
        expected_string = 'someString'
        assert self.caster._to_camel_case(example_string) == expected_string

    def test_to_camel_case_multiple_underscores(self):
        example_string = 'some_long_string_with_many_underscores'
        expected_string = 'someLongStringWithManyUnderscores'
        assert self.caster._to_camel_case(example_string) == expected_string

    def test_uri_from_credentials_empty(self):
        credentials = self.empty_credentials
        mongo_uri = self.caster._get_uri_from_credentials(credentials)
        assert mongo_uri == 'mongodb:///'

    def test_uri_from_credentials_filled_without_extras(self):
        credentials = self.filled_credentials
        mongo_uri = self.caster._get_uri_from_credentials(credentials)
        expected_string = (
            'mongodb://username:password@localhost:27017/test-database?authSource=admin&ssl=true'
        )
        assert mongo_uri == expected_string

    def test_uri_from_credentials_filled_with_extras_not_present(self):
        credentials = self.filled_credentials.copy()
        credentials['extras'] = {'replica_set': 'replica-set'}
        mongo_uri = self.caster._get_uri_from_credentials(credentials)
        expected_string = (
            'mongodb://username:password@localhost:27017/test-database?authSource=admin&ssl=true'
            '&replicaSet=replica-set'
        )
        assert mongo_uri == expected_string

    def test_uri_from_credentials_filled_with_extras_present(self):
        credentials = self.filled_credentials.copy()
        credentials['extras'] = {'ssl': True}
        mongo_uri = self.caster._get_uri_from_credentials(credentials)
        expected_string = (
            'mongodb://username:password@localhost:27017/test-database?authSource=admin&ssl=true'
        )
        assert mongo_uri == expected_string

    def test_uri_from_credentials_filled_with_extras_present_snake_case(self):
        credentials = self.filled_credentials.copy()
        credentials['extras'] = {'auth_source': 'admin'}
        mongo_uri = self.caster._get_uri_from_credentials(credentials)
        expected_string = (
            'mongodb://username:password@localhost:27017/test-database?authSource=admin&ssl=true'
        )
        assert mongo_uri == expected_string

    def test_cast_string(self):
        expected_string = 'mongodb://localhost:27017/'
        mongo_uri = self.caster.cast(expected_string)
        assert isinstance(mongo_uri, str)
        assert mongo_uri == expected_string

    def test_cast_dict(self):
        value_to_cast = {
            'username': 'test',
            'password': 'test',
            'host': 'localhost',
            'port': '27017'
        }
        expected_string = 'mongodb://test:test@localhost:27017/'
        mongo_uri = self.caster.cast(value_to_cast)
        assert isinstance(mongo_uri, str)
        assert mongo_uri == expected_string

    def test_get_value_with_uri_suffix(self):
        var_value = 'mongodb://'
        os.environ['MONGO_DB_URI'] = var_value
        assert self.caster.get_value('MONGO_DB') == var_value

    def test_get_value_without_uri_suffix(self):
        var_value = 'mongodb://'
        os.environ['MONGO_DB'] = var_value
        assert self.caster.get_value('MONGO_DB') == var_value
