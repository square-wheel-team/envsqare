from envsqare.dumpers import ListDumper


class TestListDumper:
    default_list_example = [1, 2, 3]
    default_dumper = ListDumper()

    def test_default_joiner(self):
        assert self.default_dumper.joiner == ','
        dumped_list = self.default_dumper.dump(self.default_list_example)
        assert isinstance(dumped_list, str)
        assert dumped_list == '1,2,3'

    def test_default_element_dumper(self):
        assert self.default_dumper.element_dumper == str
        dumped_list = self.default_dumper.dump(self.default_list_example)
        rejoined_list = dumped_list.split(self.default_dumper.joiner)
        assert rejoined_list, ['1', '2' == '3']

    def test_set_joiner(self):
        dumper = ListDumper()
        new_joiner = '|'
        dumper.set_joiner(new_joiner)
        assert dumper.joiner == new_joiner
        assert dumper.dump(self.default_list_example) == '1|2|3'

    def test_set_element_dumper(self):
        dumper = ListDumper()
        new_element_dumper = lambda x: f'__{x}__'
        dumper.set_element_dumper(new_element_dumper)
        assert dumper.element_dumper == new_element_dumper
        assert dumper.dump(self.default_list_example) == '__1__,__2__,__3__'

    def test_dump(self):
        dumped_list = self.default_dumper.dump(self.default_list_example)
        assert isinstance(dumped_list, str)
        assert dumped_list == '1,2,3'
