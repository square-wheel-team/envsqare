import json
import os

import pytest

from envsqare.casters import IntCaster, ListCaster
from envsqare.dotenv import dotenv
from envsqare.dotenv.classes import DotEnvVariable
from envsqare.dumpers import ListDumper
from envsqare.environment import BaseEnvironment, Environment, RawEnvironment


TEST_ENV = {
    'NUMERIC-VAR': 3,
    'LIST-VAR': [1, 2, 3, 4, 5]
}

DUMPED_TEST_ENV = {
    'NUMERIC-VAR': str(TEST_ENV['NUMERIC-VAR']),
    'LIST-VAR': ListDumper().dump(TEST_ENV['LIST-VAR'])
}
TEST_ENV_FILENAME = 'test.env'
TEST_ENV_JSON_FILENAME = 'test.json'


@pytest.fixture(autouse=True)
def clean_read_env_filenames():
    yield
    Environment.read_env_filenames = set()


@pytest.fixture
def env_file():
    with open(TEST_ENV_FILENAME, 'wb') as out_file:
        out_file.write(b'SOME_VAR=5')
    yield
    os.remove(TEST_ENV_FILENAME)


@pytest.fixture
def json_env_file():
    with open(TEST_ENV_JSON_FILENAME, 'w') as out_file:
        json.dump({'SOME_VAR': "`echo 'Hola'`"}, out_file)
    yield
    os.remove(TEST_ENV_JSON_FILENAME)


class TestBaseEnvironmentFeatures:
    env = Environment()

    def test_read_env(self, env_file):
        self.env.read_env(TEST_ENV_FILENAME)
        assert os.environ['SOME_VAR'] == '5'
        os.environ.pop('SOME_VAR')

    def test_read_env_only_once(self, env_file):
        self.env.read_env(TEST_ENV_FILENAME)
        assert TEST_ENV_FILENAME in Environment.read_env_filenames
        assert os.environ['SOME_VAR'] == '5'
        del os.environ['SOME_VAR']
        self.env.read_env(TEST_ENV_FILENAME)
        assert 'SOME_VAR' not in os.environ

    def test_read_env_only_once_false(self, env_file):
        self.env.read_env(TEST_ENV_FILENAME)
        assert TEST_ENV_FILENAME in Environment.read_env_filenames
        assert os.environ['SOME_VAR'] == '5'
        del os.environ['SOME_VAR']
        self.env.read_env(TEST_ENV_FILENAME, read_only_once=False)
        assert os.environ['SOME_VAR'] == '5'

    def test_read_env_json(self, json_env_file):
        env = Environment(json=json.load)
        env.read_env(TEST_ENV_JSON_FILENAME)
        assert os.environ['SOME_VAR'] == 'Hola'
        os.environ.pop('SOME_VAR')

    def test_get_vars_from_file(self, env_file):
        loaded_vars = self.env.get_vars_from_file(TEST_ENV_FILENAME)
        assert len(loaded_vars) == 1
        assert loaded_vars[0] == DotEnvVariable(key='SOME_VAR', value='5')

    def test_decide_file_format_default(self):
        assert self.env.decide_file_format('filename', None) is None

    def test_decide_file_format_from_filename(self):
        assert self.env.decide_file_format('.env', None) == 'env'

    def test_decide_file_format_override_filename(self):
        assert self.env.decide_file_format('env.json', '.env') == '.env'

    def test_decide_file_format_from_filename_not_dotenv(self):
        assert self.env.decide_file_format('env.json', None) == 'json'

    def test_get_format_from_filename_no_extension(self):
        assert self.env._get_format_from_filename('filename') is None  # pylint: disable=protected-access

    def test_get_format_from_filename_has_extension(self):
        assert self.env._get_format_from_filename('env.yaml') == 'yaml'  # pylint: disable=protected-access

    def test_readers_abstract(self):
        class Env(BaseEnvironment):
            casters = {}
            dumpers = {}
        with pytest.raises(TypeError):
            Env()  # pylint: disable=abstract-class-instantiated

    def test_casters_abstract(self):
        class Env(BaseEnvironment):
            readers = {}
            dumpers = {}
        with pytest.raises(TypeError):
            Env()  # pylint: disable=abstract-class-instantiated

    def test_dumpers_abstract(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {}
        with pytest.raises(TypeError):
            Env()  # pylint: disable=abstract-class-instantiated

    def test_add_readers_on_init(self):
        env = Environment(my_fileformat=lambda x: x)
        assert 'my_fileformat'.upper() in env.readers

    def test_set_var(self):
        self.env.set_var('SOME_LIST', [1, 2, 3])
        assert os.getenv('SOME_LIST') is not None
        # Check if the dumper has run
        assert os.environ['SOME_LIST'] == '1,2,3'

    def test_dump_no_dumpers(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {}
            dumpers = {}

        env = Env()
        dumped = env.dump(3)
        assert isinstance(dumped, int)
        assert dumped == 3

    def test_dump_default_dumpers(self):
        dumped = self.env.dump(3)
        assert isinstance(dumped, str)
        assert dumped == '3'

    def test_dump_custom_dumpers(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {}
            dumpers = {'list': lambda x: '-'.join(f'|{e}|' for e in x)}

        env = Env()
        dumped = env.dump([1, 2, 3])
        assert isinstance(dumped, str)
        assert dumped == '|1|-|2|-|3|'

    def test_getattr_lower(self):
        with pytest.raises(AttributeError):
            self.env.reader  # pylint: disable=pointless-statement

    def test_getattr_upper_inexistent_no_default(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {}
            dumpers = {}

        env = Env()
        with pytest.raises(AttributeError):
            env.LIST('SOME_VAR')

    def test_setattr(self):
        self.env.some_var = 5
        setattr(self.env, 'OTHER-VAR', '5')
        assert os.environ['some_var'] == '5'
        assert os.environ['OTHER-VAR'] == '5'

    def test_setattr_reserved_names(self):
        for name in BaseEnvironment.reserved_attribute_names:
            setattr(self.env, name, {})
            assert os.getenv(name) is None

    def test_call_no_default_caster(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {}
            dumpers = {}

        env = Env()
        with pytest.raises(ValueError):
            env('SOME_VAR')

    def test_call_default_caster(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {'DEFAULT': IntCaster()}
            dumpers = {}

        env = Env()
        env.SOME_VAR = '3'  # pylint: disable=invalid-name,attribute-defined-outside-init
        assert isinstance(env('SOME_VAR'), int)

    def test_call_custom_caster(self):
        class Env(BaseEnvironment):
            readers = {}
            casters = {'INT': ListCaster()}
            dumpers = {}

        env = Env()
        env.SOME_VAR = '1,2,3'  # pylint: disable=invalid-name,attribute-defined-outside-init
        casted = env.INT('SOME_VAR')
        assert isinstance(casted, list)
        assert casted == ['1', '2', '3']


class TestDefaultEnvironment:
    env = Environment()

    def test_set_list_caster_splitter(self):
        new_splitter = '|'
        self.env.set_list_caster(new_splitter)
        assert self.env.casters['LIST'].splitter == new_splitter

    def test_set_list_caster_element_caster(self):
        new_element_caster = int
        self.env.set_list_caster(element_caster=new_element_caster)
        assert self.env.casters['LIST'].element_caster == new_element_caster

    def test_set_list_caster_full(self):
        new_splitter = '|'
        new_element_caster = int
        self.env.set_list_caster(new_splitter, new_element_caster)
        assert self.env.casters['LIST'].splitter == new_splitter
        assert self.env.casters['LIST'].element_caster == new_element_caster

    def test_set_list_caster_custom_caster_key(self):
        new_splitter = '|'
        self.env.set_list_caster(new_splitter, list_caster_key='CUSTOM_KEY')
        assert self.env.casters['CUSTOM_KEY'].splitter == new_splitter

    def test_set_list_dumper_joiner(self):
        new_joiner = '|'
        self.env.set_list_dumper(new_joiner)
        assert self.env.dumpers['list'].joiner == new_joiner

    def test_set_list_dumper_element_dumper(self):
        new_element_dumper = lambda x: f'__{x}__'
        self.env.set_list_dumper(element_dumper=new_element_dumper)
        assert self.env.dumpers['list'].element_dumper == new_element_dumper

    def test_set_list_dumper_full(self):
        new_joiner = '|'
        new_element_dumper = lambda x: f'__{x}__'
        self.env.set_list_dumper(new_joiner, new_element_dumper)
        assert self.env.dumpers['list'].joiner == new_joiner
        assert self.env.dumpers['list'].element_dumper == new_element_dumper


class TestRawEnvironment:
    env = RawEnvironment()

    def test_default_loader(self):
        assert self.env.readers['DEFAULT'] == dotenv.load

    def test_dump_as_str(self):
        dumped_numeric = self.env.dump(TEST_ENV['NUMERIC-VAR'])
        dumped_list = self.env.dump(TEST_ENV['LIST-VAR'])
        assert isinstance(dumped_numeric, str)
        assert dumped_numeric == DUMPED_TEST_ENV['NUMERIC-VAR']
        assert isinstance(dumped_list, str)
        assert dumped_list == DUMPED_TEST_ENV['LIST-VAR']

    def test_read_as_str(self):
        os.environ['NUMERIC-VAR'] = self.env.dump(TEST_ENV['NUMERIC-VAR'])
        os.environ['LIST-VAR'] = self.env.dump(TEST_ENV['LIST-VAR'])
        assert isinstance(self.env('NUMERIC-VAR'), str)
        assert self.env('NUMERIC-VAR') == DUMPED_TEST_ENV['NUMERIC-VAR']
        assert isinstance(self.env.LIST('LIST-VAR'), str)
        assert self.env.LIST('LIST-VAR') == DUMPED_TEST_ENV['LIST-VAR']
