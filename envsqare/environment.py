import os
from abc import ABC, abstractmethod
from typing import Any, Callable, List

from envsqare import dotenv
from envsqare.casters import (
    BytesCaster, BoolCaster, DictFromPrefixCaster, GeneratorCaster, IntCaster, ListCaster,
    MongoCaster, StringCaster
)
from envsqare.dumpers import GenericDumper, ListDumper


DOTENV_FORMAT = ('.env', 'env', 'dotenv', 'dot-env', '', None)


class BaseEnvironment(ABC):
    reserved_attribute_names = ('readers', 'casters', 'dumpers', 'read_env_filenames')
    read_env_filenames = set()

    @property
    @abstractmethod
    def readers(self):
        """Dictionary mapping file format name to its callable reader.
        For example:
            readers = {'dotenv': dotenv.load, 'json': json.load}
            Calling env.read_env('env.json') will call json.load on the file.
        """

    @property
    @abstractmethod
    def casters(self):
        """Dictionary mapping the attribute name to use with the caster.
        For example:
            casters: {'LIST': ListCaster(), 'DEFAULT': os.getenv}
            env.LIST('SOME_LIST') will call ListCaster.
        """

    @property
    @abstractmethod
    def dumpers(self):
        """Dictionary mapping the variable class with the dumper to turn it into a string.
        For example:
            dumpers: {'int': str}
            env.set_var('SOMETHING', 3) will call str(3) and save it as an environment variable.
            All integer variables read from the environment will be casted as str(value)
        """

    def __init__(self, **readers):
        self.readers = self.readers or {}
        self.readers.update({k.upper(): v for k, v in readers.items()})

    def read_env(
        self,
        filename: str = '.env',
        reader=None,
        file_format: str = None,
        lazy: bool = False,
        read_only_once: bool = True
    ):  # pylint: disable=too-many-arguments
        if read_only_once and filename in self.__class__.read_env_filenames:
            return
        for var in self.get_vars_from_file(filename, reader, file_format, lazy):
            self._set_var(var.key, var.value)
        self.__class__.read_env_filenames.add(filename)

    def get_vars_from_file(self, filename: str = '.env', reader: callable = None, file_format: str = None,
                           lazy: bool = False) -> List[dotenv.DotEnvVariable]:
        format_to_read = self.decide_file_format(filename, file_format)
        is_dotenv = format_to_read in DOTENV_FORMAT
        if reader is None:
            reader = self.readers.get(format_to_read.upper(), self.readers['DEFAULT'])
        vars_dict = {}
        with open(filename, 'r') as env_file:
            if is_dotenv:
                vars_dict = reader(env_file, lazy)
            else:
                vars_dict = reader(env_file)
        loaded_vars = [dotenv.DotEnvVariable(key=k, value=self.dump(v)) for k, v in vars_dict.items()]
        if lazy or not is_dotenv:
            for var in loaded_vars:
                var.parse(vars_dict, is_dotenv=is_dotenv)
        return loaded_vars

    def decide_file_format(self, filename: str, file_format: str):
        return file_format or self._get_format_from_filename(filename)

    def _get_format_from_filename(self, filename: str):  # pylint: disable=no-self-use
        split_filename = filename.split('.')
        extension = split_filename[-1] if len(split_filename) > 1 else None
        return extension

    def set_var(self, key: str, value: Any):
        self._set_var(key, self.dump(value))

    def _set_var(self, key: str, value: Any):  # pylint: disable=no-self-use
        os.environ[key] = value

    def dump(self, value: Any):
        dumper = self.dumpers.get(type(value).__name__, self.dumpers.get('DEFAULT'))
        if dumper:
            value = dumper(value)
        return value

    def __getattr__(self, name: str):
        if name.isupper():
            caster = self.casters.get(name, self.casters.get('DEFAULT'))
            if caster is not None:
                return caster
        raise AttributeError(f"'{type(self).__name__}' object has no attribute '{name}'")

    def __setattr__(self, name: str, value: Any):
        if name in self.reserved_attribute_names:
            super().__setattr__(name, value)
        else:
            self.set_var(name, value)

    def __call__(self, *args, **kwargs):
        caster = self.casters.get('DEFAULT')
        if caster is not None:
            return caster(*args, **kwargs)
        raise ValueError('No default caster was supplied.')


class DotEnvReaderEnvironment(BaseEnvironment):
    readers = {'DEFAULT': dotenv.load}


class Environment(DotEnvReaderEnvironment):
    """Default environment with some nice casters you may want to use right out-of-the-box."""
    casters = {
        'DEFAULT': lambda x, default=None: os.getenv(x, default) or default,
        'BOOL': BoolCaster(),
        'BYTES': BytesCaster(),
        'GENERATOR': GeneratorCaster(),
        'INT': IntCaster(),
        'LIST': ListCaster(),
        'MONGO': MongoCaster(),
        'PREFIX': DictFromPrefixCaster(),
        'STRING': StringCaster()
    }
    dumpers = {
        'list': ListDumper(),
        'DEFAULT': GenericDumper()
    }

    def set_list_caster(self, splitter: str = None, element_caster: Callable[[str], Any] = None,
                        list_caster_key: str = 'LIST'):
        self.casters[list_caster_key] = ListCaster()
        self.casters[list_caster_key].set_splitter(splitter)
        self.casters[list_caster_key].set_element_caster(element_caster)

    def set_list_dumper(self, joiner: str = None, element_dumper: Callable[[Any], str] = None):
        self.dumpers['list'] = ListDumper()
        self.dumpers['list'].set_joiner(joiner)
        self.dumpers['list'].set_element_dumper(element_dumper)


class RawEnvironment(DotEnvReaderEnvironment):
    """Raw environment meant to be used to load and read all variables as strings.
    Even though you can technically change casters, you shouldn't.
    However, you may change readers and even dumpers to allow parsing any file format you want.
    """
    casters = {'DEFAULT': os.getenv}
    dumpers = {'DEFAULT': GenericDumper(), 'list': ListDumper()}
