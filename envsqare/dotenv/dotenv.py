from typing import Iterable

from envsqare.dotenv.classes import DotEnvVariable
from envsqare.dotenv.constants import COMMENT_CHAR, DotEnvDict
from envsqare.dotenv.exceptions import AmbiguousQuoting, UnfinishedValue


def load(file, raw: bool = False) -> DotEnvDict:
    close = False
    if isinstance(file, str):
        file = open(file, 'r')  # pylint: disable=consider-using-with
        close = True
    try:
        parsed_content = _load((line.rstrip('\n') for line in file), raw)
    finally:
        if close:
            file.close()
    return parsed_content


def loads(contents: str, raw: bool = False) -> DotEnvDict:
    lines = contents.split('\n')
    return _load(lines, raw)


def _load(lines: Iterable[str], raw: bool = False) -> DotEnvDict:
    parsed_content = {}
    for variable in _iter_vars(lines):
        if not raw:
            variable.parse(parsed_content)
        parsed_content[variable.key] = variable.value
    parsed_content.pop('', None)
    return parsed_content


def _iter_vars(lines: Iterable[str]):
    lines = lines.__iter__()
    line = next(lines, None)
    while line is not None:
        stripped = line.strip()
        if not stripped or stripped.startswith(COMMENT_CHAR):
            line = next(lines, None)
            continue
        key, value, delimiter = _parse_line(line)
        line = next(lines, None)
        while line is not None and delimiter is not None:
            _, next_value, delimiter = _parse_line(line, char_delimiter=delimiter)
            value += next_value
            line = next(lines, None)
        if line is None and delimiter is not None:
            raise UnfinishedValue(key, delimiter)
        yield DotEnvVariable(key=key, value=value)


def _parse_line(line: str, char_delimiter: str = None):
    if char_delimiter is None:
        split = line.split('=')
        key = split[0].strip()
        value = '='.join(split[1:]).strip()
        delimiter = _delimiter_expected(value)
    elif f'={char_delimiter}' in line:
        raise AmbiguousQuoting(line, char_delimiter)
    else:
        key = None
        value = line
        delimiter = None if value.endswith(char_delimiter) else char_delimiter
    return key, value, delimiter


def _delimiter_expected(value: str):
    delimiter = None
    if value.startswith('"') and not value.endswith('"'):
        delimiter = '"'
    elif value.startswith("'") and not value.endswith("'"):
        delimiter = "'"
    if value.startswith('`') and not value.endswith('`'):
        delimiter = '`'
    return delimiter
