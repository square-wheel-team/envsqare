class AmbiguousQuoting(Exception):
    """A variable was quoted ambiguously. Probably you mixed quote characters or forgot to close one."""

    DEFAULT_MESSAGE = (
        'An ambiguous quoting was detected for a variable. '
        'You may have forgotten to close a quote mark.'
    )

    def __init__(self, line: str, delimiter: str, message: str = None):
        self.line = line
        self.delimiter = delimiter
        self.message = message or self.DEFAULT_MESSAGE
        super().__init__(self.message)

    def __str__(self):
        return f'{self.message}\nDelimiter: {self.delimiter}\nLine content: {self.line} '


class UnfinishedValue(Exception):
    """You started quoting a variable but you didn't close quotes.

    For example:
        ERROR:
            * You start quoting with " but there's no double quote at the end.
            MY_VAR_NAME="I'm writing a quoted multiline
            variable
        FIX:
            * Add the closing quote mark at the end of the last line.
            MY_VAR_NAME="I'm writing a quoted multiline
                variable"

            * Writing it in a new line would also work:
            MY_VAR_NAME="I'm writing a quoted multiline
            variable
            "
    """

    DEFAULT_MESSAGE = "Couldn't find closing delimiter."

    def __init__(self, key: str, delimiter: str, message: str = None):
        self.key = key
        self.delimiter = delimiter
        self.message = message or self.DEFAULT_MESSAGE
        super().__init__(self.message)

    def __str__(self):
        return f'{self.message}\nVariable name: {self.key}\nDelimiter: {self.delimiter}'


class TerminalReplaceError(Exception):
    """An error occurred when trying to parse a variable by running commands in the terminal."""

    DEFAULT_MESSAGE = (
        'There was an error trying to get variable value from command. '
        'If your variable has spaces, try quoting it.'
    )

    def __init__(self, key: str, command: str, message: str = None):
        self.key = key
        self.command = command
        self.message = message or self.DEFAULT_MESSAGE
        super().__init__(self.message)

    def __str__(self):
        return f'{self.message}\nVariable name: {self.key}\nCommand run: {self.command}\n'
