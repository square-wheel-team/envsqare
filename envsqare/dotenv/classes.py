import os
import subprocess
import sys
from dataclasses import dataclass
from typing import Tuple, Union

from envsqare.dotenv.constants import COMMAND_SYNTAX, DotEnvDict, ENV_VAR_SYNTAX, QUOTE_CHARS
from envsqare.dotenv.exceptions import TerminalReplaceError


@dataclass
class DotEnvVariable:
    key: str
    value: str

    def parse(self, parsed_content: DotEnvDict = None, is_dotenv: bool = True):
        parsed_content = parsed_content or {}
        if self._must_parse(is_dotenv):
            self.parse_env_vars(parsed_content)
        if self._must_parse(is_dotenv, commands=True):
            self.parse_commands()
        if self._must_unwrap(is_dotenv):
            self.unwrap()

    def unwrap(self) -> str:
        self.value = self.value[1:-1]
        return self.value

    def is_wrapped(self, char: Union[str, Tuple[str]]) -> bool:
        return self.value.startswith(char) and self.value.endswith(char)

    def parse_commands(self) -> str:
        """Run commands in terminal as the system would do if running `source .env`"""
        commands = COMMAND_SYNTAX.findall(self.value)
        if commands:
            for group in commands:
                command_to_run = group[1 if group.startswith('`') else 2:-1]
                self.value = self.value[1 if group.startswith('`') else 2:-1]
                self._parse_value_from_command(command_to_run)
        elif ' ' in self.value and not self.is_wrapped(QUOTE_CHARS + ('`', )):
            self._parse_value_from_command(self.value)
        return self.value

    def parse_env_vars(self, parsed_content: DotEnvDict) -> str:
        """Replace env variables placeholders with those actually present on the system's
        environment or those placed before on the env file.
        """
        env_vars = ENV_VAR_SYNTAX.findall(self.value)
        if env_vars:
            for group in env_vars:
                var_name = group.lstrip('$').lstrip('{').rstrip('}')
                extended_value = os.getenv(var_name, str(parsed_content.get(var_name, '')))
                self.value = self.value.replace(group, extended_value)
        return self.value

    def _must_parse(self, is_dotenv: bool = True, commands: bool = False):
        parse_dotenv = self.is_wrapped("'")
        parse_not_dotenv = self.is_wrapped('`' if commands else ('"', '`'))
        return is_dotenv and parse_dotenv or not is_dotenv and parse_not_dotenv

    def _must_unwrap(self, is_dotenv: bool = True):
        unwrap_dotenv = is_dotenv and self.is_wrapped(QUOTE_CHARS + ('`', ))
        unwrap_not_dotenv = self.is_wrapped(('"', '`'))
        return (is_dotenv and unwrap_dotenv) or (not is_dotenv and unwrap_not_dotenv)

    def _parse_value_from_command(self, command: str):
        try:
            extended_value = subprocess.check_output(
                command,
                shell=True
            ).strip().decode(sys.stdout.encoding)
        except Exception as e:
            raise TerminalReplaceError(self.key, command) from e
        else:
            self.value = self.value.replace(command, extended_value)
