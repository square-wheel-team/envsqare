import re
from typing import Dict


ENV_VAR_SYNTAX = re.compile(r'(\${?[a-zA-Z0-9-_]+}?)')
COMMAND_SYNTAX = re.compile(r'(\$\(.*\)|`.*`)')
COMMENT_CHAR = '#'
QUOTE_CHARS = ('"', "'")
DotEnvDict = Dict[str, str]
