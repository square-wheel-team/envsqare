import os
from abc import ABC, abstractmethod
from typing import Iterable

import toml

from envsqare.dotenv.constants import DotEnvDict


class Reader(ABC):  # pylint: disable=too-few-public-methods
    @abstractmethod
    def __call__(self, filename: str, *args, **kwargs) -> DotEnvDict:
        """Any callable may be a reader, even a function."""


class TOMLReader(ABC):  # pylint: disable=too-few-public-methods
    DEFAULT_PREFIX_FREE_SECTIONS = ('Main', 'MAIN', 'main')
    DEFAULT_PREFIX_SPLITTER = '_'

    def __init__(self, prefix_free_sections: Iterable[str] = None, prefix_splitter: str = None):
        prefix_free_sections = prefix_free_sections or self.DEFAULT_PREFIX_FREE_SECTIONS
        self.avoid_prefix = prefix_free_sections
        self.prefix_splitter = prefix_splitter or self.DEFAULT_PREFIX_SPLITTER

    def __call__(self, filename: str):
        loaded_data, output_data = {}, {}
        if os.path.exists(filename):
            with open(filename, 'r') as env_file:
                loaded_data = toml.load(env_file)
        else:
            loaded_data = toml.loads(filename)
        for key, value in loaded_data.items():
            if isinstance(value, dict):
                prefix = f'{key}{self.prefix_splitter}' if key not in self.avoid_prefix else ''
                for sub_key, sub_value in value.items():
                    output_data[f'{prefix}{sub_key}'] = sub_value

    def _load(self, env_file):  # pylint: disable=no-self-use
        if isinstance(env_file, str):
            with open(env_file, 'r') as env_file_obj:
                loaded_data = toml.load(env_file_obj)
        else:
            loaded_data = toml.load(env_file)
        return loaded_data

    def _loads(self, toml_string: str):  # pylint: disable=no-self-use
        return toml.loads(toml_string)
