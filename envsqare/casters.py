import os
from abc import ABC, abstractmethod
from dataclasses import dataclass, field
from enum import Enum
from typing import Any, Callable, Dict, Generator, List, Optional, Set, Union


DEFAULT_ENCODING = 'utf-8'
MISSING_VARNAME_ERROR = "__call__() missing 1 required positional argument: 'var_name'"


class Caster(ABC):
    DEFAULT_VAR_NAME = None

    def __call__(self, var_name: str = None, default_value: Optional[Any] = None):
        var_name = var_name or self.DEFAULT_VAR_NAME
        if var_name is None:
            raise TypeError(MISSING_VARNAME_ERROR)
        value = self.get_value(var_name)
        if value is not None:
            value = self.cast(value)
        return value or default_value

    @abstractmethod
    def cast(self, value: str):
        """Convert value into the type you want."""

    def get_value(self, var_name: str):  # pylint: disable=no-self-use
        return os.getenv(var_name)


@dataclass
class BoolCaster(Caster):
    truthy_values: Set[str] = field(default_factory=lambda: {'true', 't', 'yes', 'y', '1'})

    def cast(self, value) -> bool:
        return (value or '').lower() in self.truthy_values


@dataclass
class IntCaster(Caster):
    base: int = 10

    def cast(self, value) -> int:
        return int(value, self.base)


@dataclass
class StringCaster(Caster):
    encoding: str = DEFAULT_ENCODING

    def cast(self, value) -> str:
        if isinstance(value, bytes):
            casted = value.decode(self.encoding)
        else:
            casted = str(value)
        return casted


@dataclass
class BytesCaster(Caster):
    encoding: str = DEFAULT_ENCODING

    def cast(self, value) -> bytes:
        return f'{value}'.encode(self.encoding)


@dataclass
class ListCaster(Caster):
    splitter: str = ','
    element_caster: Callable[[str], Any] = str

    def cast(self, value: str) -> List[Any]:
        return [self.element_caster(v) for v in value.split(self.splitter)]

    def set_splitter(self, splitter: Union[str, None]):
        self.splitter = splitter or self.splitter

    def set_element_caster(self, element_caster: Union[Callable[[str], Any], None]):
        self.element_caster = element_caster or self.element_caster


@dataclass
class GeneratorCaster(ListCaster):
    def cast(self, value) -> Generator[Any, None, None]:
        return (v for v in super().cast(value))


@dataclass
class DictFromPrefixCaster(Caster):
    prefix_splitter: str = '_'

    def get_value(self, var_name: str):
        prefix_with_splitter = f'{var_name}{self.prefix_splitter}'
        value = {
            prefix_with_splitter.join(v.split(prefix_with_splitter)[1:]): os.environ[v] for v in os.environ
            if v.startswith(prefix_with_splitter)
        }
        return value

    def cast(self, value) -> dict:
        return value


class MongoCredentials(Enum):
    USERNAME = 0
    PASSWORD = 1
    HOST = 2
    PORT = 3
    DATABASE = 4
    PARAMS = 5


@dataclass
class MongoCaster(DictFromPrefixCaster):
    DEFAULT_VAR_NAME = 'MONGO'  # pylint: disable=invalid-name

    names: Dict[int, List[str]] = field(default_factory=lambda: {
        MongoCredentials.USERNAME: ['user', 'username'],
        MongoCredentials.PASSWORD: ['password', 'pass', 'pwd', 'passwd'],
        MongoCredentials.HOST: ['host', 'server', 'server_name', 'servername'],
        MongoCredentials.PORT: ['port', 'host_port', 'hostport'],
        MongoCredentials.DATABASE: ['database', 'db', 'base'],
        MongoCredentials.PARAMS: ['params', 'parameters', 'arguments', 'options']
    })

    def get_value(self, var_name: str):
        value = os.getenv(f'{var_name}{self.prefix_splitter}URI')
        value = value or super().get_value(var_name)
        return value or None

    def cast(self, value) -> str:
        if isinstance(value, dict):
            value = {k.lower(): v for k, v in value.items()}
            credentials = {}
            for cred_type in MongoCredentials:
                credentials[cred_type] = self._get_from_name(cred_type, value)
            credentials['extras'] = value
            mongo_uri = self._get_uri_from_credentials(credentials)
        else:
            mongo_uri = value
        return mongo_uri

    def _get_from_name(self, var_type: str, value_dict: Dict[str, str]) -> str:
        ret = None
        keys = value_dict.keys()
        for name in self.names[var_type]:
            if name in keys:
                ret = value_dict.pop(name)
                break
        return ret

    def _get_uri_from_credentials(self, credentials: Dict[str, str]) -> str:
        # pylint: disable=no-self-use
        mongo_uri = f"mongodb://{credentials[MongoCredentials.USERNAME] or ''}"
        if credentials[MongoCredentials.PASSWORD]:
            mongo_uri += f':{credentials[MongoCredentials.PASSWORD]}'
        if credentials[MongoCredentials.HOST]:
            mongo_uri += f'@{credentials[MongoCredentials.HOST]}'
        if credentials[MongoCredentials.PORT]:
            mongo_uri += f':{credentials[MongoCredentials.PORT]}'
        mongo_uri += '/'
        if credentials[MongoCredentials.DATABASE]:
            mongo_uri += credentials[MongoCredentials.DATABASE]

        params = {}
        if credentials[MongoCredentials.PARAMS]:
            provided_params = credentials[MongoCredentials.PARAMS].split('&')
            split_params = ((k, v) for k, v in (p.split('=') for p in provided_params))
            for param_name, param_value in split_params:
                if param_name not in params:
                    params[param_name] = param_value

        extras = credentials.get('extras')
        if extras:
            for param_name, param_value in extras.items():
                if param_name not in params:
                    params[self._to_camel_case(param_name)] = param_value

        params_string = ''
        for param_name, param_value in params.items():
            splitter = '' if param_name.startswith('&') else '&'
            params_string += f'{splitter}{self._to_camel_case(param_name)}={param_value}'
        params_string = params_string[1:] if params_string.startswith('&') else params_string
        if params_string:
            mongo_uri += f'?{params_string}'
        return mongo_uri

    def _to_camel_case(self, underscored_string: str) -> str:  # pylint: disable=no-self-use
        words = underscored_string.split('_').__iter__()
        camel_string = next(words)
        word = next(words, None)
        while word is not None:
            camel_string += word.capitalize()
            word = next(words, None)
        return camel_string
