# pylint: disable=too-few-public-methods,no-self-use
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any, Callable, List, Union


class Dumper(ABC):
    def __call__(self, value):
        return self.dump(value)

    @abstractmethod
    def dump(self, value: Any) -> str:
        """Convert value into a dotenv valid string."""


class GenericDumper(Dumper):
    def dump(self, value: Any) -> str:
        return str(value)


@dataclass
class ListDumper(Dumper):
    joiner: str = ','
    element_dumper: Callable[[Any], str] = str

    def dump(self, value: List[Any]) -> str:
        return self.joiner.join([self.element_dumper(v) for v in value])

    def set_joiner(self, joiner: Union[str, None]):
        self.joiner = joiner or self.joiner

    def set_element_dumper(self, element_dumper: Union[Callable[[Any], str], None]):
        self.element_dumper = element_dumper or self.element_dumper
